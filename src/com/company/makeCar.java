package com.company;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

public class makeCar {
    private int nXvalue;
    private int nYvalue;
    public makeCar(int x , int y){
        this.nXvalue = x;
        this.nYvalue= y;

    }
    public void draw(Graphics2D g2){
        Rectangle body = new Rectangle(this.nXvalue, this.nYvalue+10,60,10);
        Ellipse2D.Double fTire = new Ellipse2D.Double(this.nXvalue+10, this.nYvalue+20,10,10);
        Ellipse2D.Double rTire = new Ellipse2D.Double(this.nXvalue+40, this.nYvalue+20,10,10);
        //start of windshield
        Point2D.Double r1 = new Point2D.Double(this.nXvalue + 10, this.nYvalue+10);
        //windshield
        Point2D.Double r2 = new Point2D.Double(this.nXvalue+20, this.nYvalue);
        //back window
        Point2D.Double r3= new Point2D.Double(this.nXvalue+40, this.nYvalue);
        //end of back window
        Point2D.Double r4 = new Point2D.Double(this.nXvalue+50, this.nYvalue+10);

        Line2D.Double windShield = new Line2D.Double(r1,r2);
        Line2D.Double roof = new Line2D.Double(r2,r3);
        Line2D.Double rearWS = new Line2D.Double(r3,r4);
        g2.draw(body);
        g2.draw(fTire);
        g2.draw(rTire);
        g2.draw(windShield);
        g2.draw(roof);
        g2.draw(rearWS);
    }
}
