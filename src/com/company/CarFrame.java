package com.company;

import javax.swing.*;
import java.awt.event.ActionListener;
import javax.swing.Timer;
import javax.swing.JFrame;
import java.awt.event.ActionEvent;

/**
 * class CarFrame to extend JFrame
 */
public class CarFrame extends JFrame {
    //Size of window and starting values for x and y
    private int nWidth = 300;
    private int nHeight = 400;
    private int startingX = 0;
    private int startingY = 0;
   // instance of CarComponent
    private CarComponent moveCar;
   //instance of Timer class
    private Timer t;

    /**
     * constructor for CarFrame
     */
    public CarFrame() {
        //create a car and add it into the frame
        moveCar = new CarComponent(startingX, startingY);
        add(moveCar);
        //set size of frame
        setSize(nWidth, nHeight);
        //add a timer with 100ms delay
        ActionListener listener = new timer();
        final int nDELAY = 100;
        t = new Timer(nDELAY, listener);

        //start the timer
        t.start();


    }

    /**
     * class timer to implement ActionListener
     */
    class timer implements ActionListener {
        /**
         * override actionPerformed method
         * @param event
         */
        public void actionPerformed(ActionEvent event) {
            //if x is less than frame's width
            if (startingX < getWidth()) {
                //increment position
                startingX += 30;
                startingY += 30;
                //move car by changing origin and repaint
                moveCar.moveCarBy(startingX, startingY);
                //add car to frame
                add(moveCar);
            } else {
                //if the car already passes the width, stop the timer
                t.stop();
            }
        }
    }

    /**
     * return current X coordinate
     * @return this.startingX
     */
    public int getStartingX(){
        return this.startingX;
    }
    /**
     * return current Y coordinate
     * @return this.startingY
     */
    public int getStartingY(){
        return this.startingY;
    }
}



