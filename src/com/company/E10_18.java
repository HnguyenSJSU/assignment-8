package com.company;
/**
 * Hung Nguyen
 * 013626210
 * Create a frame with 2 buttons each with their own increment
 */

import java.awt.*;
import java.awt.event.ActionListener;
import javax.swing.*;


public class E10_18 {

    public static void main(String[] args) {
        //create a frame
        JFrame frame = new JFrame();
        //create 2 buttons
        JButton button1 = new JButton("Click me!");

        JButton button2 = new JButton("Click me!");
        //add buttons to Jpanel for organization
        JPanel buttonPannel = new JPanel();
        buttonPannel.add(button1);
        buttonPannel.add(button2);
        //add panel into frame
        frame.add(buttonPannel, BorderLayout.CENTER);
        //Listen for any clicks
        ActionListener listener = new ClickListener(button1, button2);
        //add buttons to listen
        button1.addActionListener(listener);
        button2.addActionListener(listener);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(100,100);
        frame.setVisible(true);
    }
}

/**
 * Button 1: I was clicked 1 times
 * Button 1: I was clicked 2 times
 * Button 1: I was clicked 3 times
 * Button 1: I was clicked 4 times
 * Button 2: I was clicked 1 times
 * Button 2: I was clicked 2 times
 * Button 2: I was clicked 3 times
 * Button 2: I was clicked 4 times
 * Button 2: I was clicked 5 times
 *
 * Process finished with exit code 130
 */