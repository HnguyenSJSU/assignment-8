package com.company;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * class that inherits JFrame
 */
public class InvestmentFrame extends JFrame {
    //frame width and height
    private static final int FWIDTH = 450;
    private static final int FHEIGHT = 450;
    //textfield width
    private static final int FIELDWIDTH = 10;
    //Labels, textfields, and buttons declared
    private JLabel Labelrate;
    private JButton submitButton;
    private JLabel LabelYrs;
    private JLabel LabelInit;
    private JLabel LabelResult;
    private JLabel LabelError;
    private JLabel LabelError2;
    private JLabel LabelError3;
    private JTextField initAmountField;
    private JTextField annualInterestField;
    private JTextField numberOfYearsField;

    //store balance calculated
    private double dBalance;
    private Pattern p = Pattern.compile("[^0-9 ]", Pattern.CASE_INSENSITIVE);
    //consturctor for frame
    public InvestmentFrame(){
        //set frame size
        setSize(FWIDTH,FHEIGHT);
        //panel for labels and textfields
        JPanel panel = new JPanel();
        //create text field for balance
        createTextField(1);

        panel.add(this.LabelInit);
        panel.add(this.initAmountField);
        //create text field for interest
        createTextField(2);
        panel.add(this.Labelrate);
        panel.add(this.annualInterestField);
        //create text field for years
        createTextField(3);
        panel.add(this.LabelYrs);
        panel.add(this.numberOfYearsField);
        //create button to submit
        createButton();
        panel.add(this.submitButton);

        //panel for result and error
        JPanel panel2 = new JPanel();
        LabelResult = new JLabel("Balance = 0");
        panel2.add(LabelResult);
        LabelError = new JLabel("");
        LabelError2 = new JLabel("");
        LabelError3 = new JLabel("");
        panel2.add(LabelError);
        panel2.add(LabelError2);
        panel2.add(LabelError3);
        add(panel, BorderLayout.CENTER);
        add(panel2, BorderLayout.NORTH);
    }

    /**
     * create text fields for chosen fields
     * @param num
     */
    private void createTextField(int num){
        //create fields for balance
        if (num ==1 ){
            this.LabelInit  = new JLabel("Initial balance");
            this.initAmountField = new JTextField(this.FIELDWIDTH);
        }
        //create fields for Interest rate
        if (num == 2){
            this.Labelrate  = new JLabel("Interest rate");
            this.annualInterestField = new JTextField(this.FIELDWIDTH);
        }
        //create fields for Years invested
        if (num == 3){
            this.LabelYrs  = new JLabel("Years Invested");
            this.numberOfYearsField = new JTextField(this.FIELDWIDTH);
        }
    }

    /**
     * Create button for submission
     */
    private void createButton(){
        this.submitButton = new JButton("Submit");
        ActionListener listener = new buttonListen();
        this.submitButton.addActionListener(listener);
    }

    /**
     * nested class to override ActionListener
     */
    class buttonListen implements ActionListener{
        /**
         * override actionPerformed to calculate balance and interests
         * @param event
         */
        public void actionPerformed(ActionEvent event) {
            try {
                  //get double from fields
                  dBalance = getText(1);
                  double dRate = getText(2);
                  double dYear = getText(3);

                //for years, calculate the interests and add them onto balance
                for (int nIndx = 1; nIndx <= dYear; nIndx++) {
                    double dInterest = (dBalance * dRate * nIndx) / 100;
                    System.out.println(dInterest);

                    dBalance += dInterest;
                }
                //update result
                LabelResult.setText(String.format("Balance after %.0f year is $%.2f", dYear, dBalance));
            }
            //catch exception if numbers cannot be parsed
            catch(NumberFormatException exception){
                LabelResult.setText("Please enter all numbers");
                if(dBalance != 0){
                    dBalance = 0;
                    LabelResult.setText(String.format("Balance: 0,"));
                }
            }
        }
    }

    /**
     * get text on each fields
     * @param choice
     * @return balance
     */
    public Double getText(int choice){
        String Balance = "";
        //if choice == 1, get text for initial balance
        if (choice == 1){
            Balance = initAmountField.getText();
            Matcher m = p.matcher(Balance);
            boolean b = m.find();
            //try to parse text to double
            try {
                Double temp = Double.parseDouble(Balance);
            }//if there exists text or symbols
            catch(NumberFormatException e){
                //update labelerror
                LabelError.setText(String.format("Letters and special character in Balance field: %s is not valid", Balance));

            }
        }
        //if choice == 2, get text for interest
        else if (choice == 2){
            Balance = annualInterestField.getText();
            //try to parse text to double
            try {
                Double temp = Double.parseDouble(Balance);
            }
            catch(NumberFormatException e){
                //update labelerror
                LabelError2.setText(String.format("Letters and special character in Interest field: %s is not valid", Balance));

            }
        }
        //if choice == 3, get text for years field
        else if (choice == 3){
            Balance = numberOfYearsField.getText();
            //try to parse text to double
            try {
                Double temp = Double.parseDouble(Balance);
            }//update labelerror
            catch(NumberFormatException e){
                LabelError3.setText(String.format("Letters and special character in Years field: %s is not valid", Balance));

            }
        }

        return Double.parseDouble(Balance);
    }



}
