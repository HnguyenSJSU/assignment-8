package com.company;

import javax.swing.*;

/**
 * Hung Nguyen 013626210
 * Create a moving car from 0,0 to x,y
 */
public class P10_22 {
    public static void main(String[] args) {
        //create a carFrame
        JFrame frame = new CarFrame();
        frame.setTitle("Moving car");
        //exit upon closing
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);


    }
}
