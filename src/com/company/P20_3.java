package com.company;

import javax.swing.*;

public class P20_3 {
    public static void main(String[] args) {
        //create an investment frame
        JFrame frame = new InvestmentFrame();
        frame.setTitle("Investment");
        //close on exit
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.setVisible(true);
    }

}
