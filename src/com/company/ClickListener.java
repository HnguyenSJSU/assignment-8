package com.company;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * ClickListener class to listen for events
 */
public class ClickListener implements ActionListener {
    //store counters for each button
    private int nCount1 = 0;
    private int nCount2 = 0;
    //instance of JButton
    private JButton Button1, Button2;

    /**
     * ClickListener constructor
     * @param b1
     * @param b2
     */
    public ClickListener(JButton b1, JButton b2){

        this.Button1=b1;
        this.Button2=b2;
    }

    /**
     * Listen to events and increment counter with each click
     * @param event
     */
    public void actionPerformed(ActionEvent event){
        //if the button1 is clicked
        if(event.getSource()== this.Button1){
            this.nCount1++;
            System.out.println(String.format("Button 1: I was clicked %d times", this.nCount1));
        } //if button2 is clicked
        else {
            this.nCount2++;
            System.out.println(String.format("Button 2: I was clicked %d times", this.nCount2));

        }

    }
}