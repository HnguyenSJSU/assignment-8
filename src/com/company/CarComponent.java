package com.company;

import javax.swing.*;
import java.awt.*;

/**
 * CarComponent class to inherit JComponent
 */
public class CarComponent extends JComponent {
    //create an instance of makeCar
    private makeCar car1;
    //constructor for CarComponent
    public CarComponent(int x, int y){
        car1 = new makeCar(x,y);
    }

    /**
     * paintComponent to draw the car
     * @param g
     */
    public void paintComponent(Graphics g){
//        makeCar car1 = new makeCar (0,0);
        Graphics2D g2 = (Graphics2D) g;
        car1.draw(g2);
    }

    /**
     * change origin of x and y for car
     * @param x
     * @param y
     */
    public void moveCarBy(int x, int y){
        car1 = new makeCar(x,y);
        repaint();
    }

}
